#mid-domain effect
# let's assume that we're talking about a terrestrial clade or guild that is mostly made up of things that don't tolerate more than a few days in a row of below-freezing temperatures. So they can't live most places further from the equator than, say 35N or 35S. (this is about the latitudinal extent of Africa; on the US this corresponds to North Carolina, Norman, OK, and about Santa Monica CA; Canberra, Australia or Tokyo, Japan)
# latitudinal ranges are drawn from a distribution of lengths, ranging from 1 - 70d
# to simulate this, let's pick a series of random numbers that range from -35 to 35, and use that as one extreme of the range, and then pick a series of random number from -35 to 35 and use those as the other extreme of the range: 

# if you haven't already: 
#install.packages("ggplot2")
#install.packages("lattice") # for densityplot
# install.packages("msir")  # for loess with st.dev. 
library(ggplot2)
library(lattice)
library(msir)


# generate two vectors of random numbers between -35 and 35

pointOne = runif(1000, min = -35, max = 35)
pointTwo = runif(1000, min = -35, max = 35)

# for each position of the two vectors above, use the lower number for the "minLat" vector, and the higher number for the "maxLat" vector

minLat = pmin(pointOne, pointTwo)
maxLat = pmax(pointOne, pointTwo)

#from the two vectors above, create a latitudinal midpoint and range
midLat = (maxLat + minLat)/2
rangeLat = (maxLat - minLat)

# make a dataframe we can play with moving forward out of the previous four vectors 
smallPointsJoined = data.frame(minLat = minLat, maxLat = maxLat, midLat = midLat, rangeLat = rangeLat)

forLoessSmall = data.frame(midLat = midLat, rangeLat = rangeLat)

# latitudinal range as a function of latitudinal midpoint. 
# cf. Rapoport's "rule"
# loess is a smoothed way of visualizing nonlinear trends in data
# the dotted lines are +/- 1 st. dev. of the loess approximation
dev.new()
plot(forLoessSmall, pch = 16, col = rgb(0, 1, 1, alpha=0.6), main = "latitudinal range as a function of latitudinal midpoint", xlab = "latitudinal midpoint", ylab = "latitudinal range")
lines(l <- loess.sd(forLoessSmall)); lines(l$x, l$upper, lty=2); lines(l$x, l$lower, lty=2)

dev.new()
densityplot(smallPointsJoined$midLat, main = "probability distribution function of latitudinal midpoints")

dev.new()
plot(smallPointsJoined$minLat ~ smallPointsJoined$midLat, ylim = c(-36, 36), xlim = c(-36, 36), col = "blue", main = "maxium (red) and minimum (blue) latitudinal limit as a function of latitudinal midpoint")
points(smallPointsJoined$maxLat ~ smallPointsJoined$midLat, col = "darkred")

dev.new()
densityplot(smallPointsJoined$midLat, main = "probability distribution function of latitudinal midpoints")


# This is just a better way of showing the data in the red and blue plot
dev.new()
f <- ggplot(smallPointsJoined, aes(x = smallPointsJoined$midLat, y = smallPointsJoined$midLat,
                     ymin=smallPointsJoined$minLat, ymax=smallPointsJoined$maxLat)) + labs(title = "latitudinal range as a function of latitudinal midpoint")

# Line range
f + geom_linerange()

                     


# how many "species ranges" intersect each latitudinal integer in our range? 
binsSmall = -35:35
binCountsSmall = rep(0, 71)


for (i in -35:35){
		binCountsSmall[i + 36] = dim(smallPointsJoined[which(smallPointsJoined$minLat < i  & smallPointsJoined$maxLat > i), ])[1]
}

smallBinsJoined = data.frame(bins = binsSmall, binCounts = binCountsSmall)

dev.new()
plot(smallBinsJoined, ylab = "species richness at this latitudinal band", xlab = "latitudinal intersection point", main = "Latitudinal gradient of species richness")
## compare this plot to the densityplot of latitudinal midpoints generated above






# it works the same way if the maximum latitude possible is 90d

pointOne = runif(500, min = -90, max = 90)
pointTwo = runif(500, min = -90, max = 90)
minLat = pmin(pointOne, pointTwo)
maxLat = pmax(pointOne, pointTwo)
midLat = (maxLat + minLat)/2
rangeLat = (maxLat - minLat)

largePointsJoined = data.frame(minLat = minLat, maxLat = maxLat, midLat = midLat, rangeLat = rangeLat)

dev.new()
plot(largePointsJoined$rangeLat ~ largePointsJoined$midLat)

dev.new()
hist(largePointsJoined$midLat)
dev.new()
plot(largePointsJoined$minLat ~ largePointsJoined$midLat, ylim = c(-95, 95), xlim = c(-95, 95), col = "blue")
points(largePointsJoined$maxLat ~ largePointsJoined$midLat, col = "darkred")

dev.new()
densityplot(largePointsJoined$midLat)


dev.new()
f = ggplot(largePointsJoined, aes(x = largePointsJoined$midLat, y = largePointsJoined$midLat,
                     ymin=largePointsJoined$minLat, ymax=largePointsJoined$maxLat))

# Line range
f + geom_linerange()

                     







binsLarge = -90:90
binCountsLarge = rep(0, 181)




for (i in -90:90){
		binCountsLarge[i + 91] = dim(largePointsJoined[which(largePointsJoined$minLat < i  & largePointsJoined$maxLat > i), ])[1]
}

binsJoinedLarge = data.frame(bins = binsLarge, binCounts = binCountsLarge)
dev.new()
plot(binsJoinedLarge, ylab = "species richness at this latitudinal band", xlab = "latitudinal intersection point")


### what happens if some members of a clade can potentially adapt to all climates, but most members of a clade are limited by their environmental tolerances? 

allPointsJoined = rbind(largePointsJoined, smallPointsJoined)
binsAll = -90:90
binCountsAll = rep(0, 181)

for (i in -90:90){
		binCountsAll[i + 91] = dim(allPointsJoined[which(allPointsJoined$minLat < i  & allPointsJoined$maxLat > i), ])[1]
}

binsJoinedAll = data.frame(bins = binsAll, binCounts = binCountsAll)

dev.new()
plot(binsJoinedAll, ylab = "species richness at this latitudinal band", xlab = "latitudinal intersection point", main = "some species can only tolerate warm temperatures, \n but some can tolerate extremes")