# you probably already did the function install.packages("vegan", dependencies = TRUE)
# you might not have installed "zetadiv" yet, though, so: install.packages("zetadiv", dependencies = TRUE)

library(vegan)
library(zetadiv)

### for this exercise we'll be loading in read data on a land snail genus from Palau called "Hungerfordia"
### so you'll need to read in several files of data
### make sure you use the setwd("insert/file/path/in/quotation/marks") command to tell R where approximately to look for these files
### on a mac, you may just be able to drag and drop the files into the quotation marks for read.table(file = "")

### in the commands below, I'm assuming that you've downloaded all of the relevant files into the the same folder you specified in "setwd()"
### the files are tab delimited (as opposed to another popular format, .csv, or "comma-separated values"), 
### so use the sep = "\t" to tell R what to look for (if it was a .csv file, you'd use sep = ",")

all_islandData = read.table(file = "all_islandData.txt", header = TRUE, sep = "\t")
all_hungerfordia = read.table(file = "all_hungerfordiaAssemblages.txt", header = TRUE, sep = "\t")

south_islandData = read.table(file = "southern_islandData.txt", header = TRUE, sep = "\t")
south_hungerfordia = read.table(file = "southern_hungerfordiaAssemblages.txt", header = TRUE, sep = "\t")

north_islandData = read.table(file = "northern_islandData.txt", header = TRUE, sep = "\t")
north_hungerfordia = read.table(file = "northern_hungerfordiaAssemblages.txt", header = TRUE, sep = "\t")

### as you might have guessed, the _hungerfordia tables are community tables similar to what you generated in the species diversity playground exercizes, 
### except they're presence - absence data (collections weren't really standardized by area or effort, so p/a is more conservative)
### rather than a square sub-sample of a larger square grid, each row in the "_hungerfordia" tables combines all of the sampling from one island
### the _islandData files contain associated data from each island that corresponds to the rows in the community matrices

### as you might guess, the "north_ " and "south_ " files have subsets of the data from the northern and southern part of the archipelago
### see poster here for an overview https://drive.google.com/open?id=0B6zTUTtOwaSMWk5ock5YcTBveTQ 

### the islandData files include a column with island species richness, so you can use that data table alone to look at the species-area relationship

plot(all_islandData$richness ~ all_islandData$areaKm2 , main = "Species- area relationship of \n Hungerfordia in Palau ", ylab = "species richness", xlab = "island area")

### usually, though, species-area relationships are depicted in log-log space, so a better graph would be: 

plot(log10(all_islandData$richness) ~ log10(all_islandData$areaKm2) , main = "Species- area relationship of \n Hungerfordia in Palau ", ylab = "log species richness", xlab = "log island area")

### but we're interested in beta diversity here, so let's get back on track

### one way of thinking about beta diversity is like a distance, but it's not just distance in x-y (two-dimensional) space—
### it's potentially distance in n-dimensional space, where n is the number of species
### the help file for the "vegdist" function in vegan has a ton of information about different ways of quantifying the dissimilarily (or distance) of assemblages
### so check out ?vegdist
### I'll wait
### a nice way of visualizing high-dimensional data in a way we can display and understand is ordination
### ordination includes a bunch of techniques; for a good overview, check here: http://ordination.okstate.edu/overview.htm 
### and you can also check out the help files for the vegan functions ?metaMDS , ?cca , and perhaps others. 
### we're going to skip straight to doing a non-metric multidimensional scaling approach using the metaMDS function

NMDSbinomial = metaMDS(all_hungerfordia, distance = "binomial", trymax = 9999)  
plot(NMDSbinomial, type = "points", main = "NMDS of binomial distance among all \n Hungerfordia assemblages") 
ordihull(NMDSbinomial, all_islandData$islandGroup, col = "blue") 
ordilabel(NMDSbinomial, label = all_islandData $IslandName)

### the ordihull function connects islands that I (somewhat subjectively) placed in geographic groups
### the ordilabel function labels each island

### you might notice that some islands seem really distant from each other in ordination space (probably Babeldaob and Ngeruktabel)

### beta-diversity distance metrics sometimes behave kind of weirdly when you include sites that don't share any species with any other sites
### they also don't always know what to do with sites with only one species
### in this case, I happen to know that there are a few islands with only one species, including a few with single-island endemics
### so let's make a subset of islands that only have 3 or more species, just to be safe

threePlus_hungerfordia = read.table(file = "threePlus_hungerfordiaAssemblages.txt", header = TRUE, sep = "\t")
threePlus_islandData = read.table(file = "threePlus_islandData.txt", header = TRUE, sep = "\t")

NMDS3plusBinomial = metaMDS(threePlus_hungerfordia, distance = "binomial", trymax = 9999)
plot(NMDS3plusBinomial, type = "points", main = "NMDS of binomial distance among \n Hungerfordia assemblages with three or more spp.")
ordihull(NMDS3plusBinomial, threePlus_islandData$islandGroup, col = "blue")
ordilabel(NMDS3plusBinomial, label = threePlus_islandData$IslandName)

### with this constrained assemblage, we can experiment with how different distance metrics might influence the ordination
NMDS3plusJaccard = metaMDS(threePlus_hungerfordia, distance = "jaccard", trymax = 9999)
plot(NMDS3plusJaccard, type = "points", main = "NMDS of Jaccard distance among \n Hungerfordia assemblages with three or more spp.")
ordihull(NMDS3plusJaccard, threePlus_islandData$islandGroup, col = "blue")
ordilabel(NMDS3plusJaccard, label = threePlus_islandData$IslandName)

NMDS3plusChao = metaMDS(threePlus_hungerfordia, distance = "chao", trymax = 9999)
plot(NMDS3plusChao, type = "points", main = "NMDS of Chao distance among \n Hungerfordia assemblages with three or more spp.")
ordihull(NMDS3plusChao, threePlus_islandData$islandGroup, col = "blue")
ordilabel(NMDS3plusChao, label = threePlus_islandData$IslandName)
### check out ?vegdist for why Chao and Jaccard distance plots might look similar

NMDS3plusRaup = metaMDS(threePlus_hungerfordia, distance = "raup", trymax = 9999)
plot(NMDS3plusRaup, type = "points", main = "NMDS of Raup-Crick distance among \n Hungerfordia assemblages with three or more spp.")
ordihull(NMDS3plusRaup, threePlus_islandData$islandGroup, col = "blue")
ordilabel(NMDS3plusRaup, label = threePlus_islandData$IslandName)

### in general, you might notice that, even though some of the distance metrics are really closely related (or synonymous)
### similar groupings of islands seem to be popping up
### furthermore, the distance metrics seem to stack some island groups on top of each other: Peleliu often ends up really close to Ulong and Mecherchar, among others

### as it happens, the islands that tend to get jumbled up on top of each other are mostly in the southern part of the lagoon, and the ones that seem to separate out consistently are mostly a bit further north

### let's check these island groups out separately to see if they differ in other ways of quantifying diversity
threePlusSouth_hungerfordia = read.table(file = "threePlus_southern_hungerfordiaAssemblages.txt", header = TRUE, sep = "\t")
threePlusSouth_islandData = read.table(file = "threePlus_southern_islandData.txt", header = TRUE, sep = "\t")

threePlusNorth_hungerfordia = read.table(file = "threePlus_northern_hungerfordiaAssemblages.txt", header = TRUE, sep = "\t")
threePlusNorth_islandData = read.table(file = "threePlus_northern_islandData.txt", header = TRUE, sep = "\t")

### one way of figuring out if it's geography that's driving the beta diversity patterns is looking at spatial autocorrelation
### when assemblages that are geographically close have similar species, and geographically distant have more distant species
### these species assemblages are said to be spatially autocorrelated
### a way of statistically testing this is with a Mantel Test, which is implemented in vegan with the function "mantel.correlog"
### mantel tests quantify the relationship between two distance matrices
### in our case: beta diversity space (output from a vegdist function on species assemblages); 
### and geographic space (output of a regular "dist" function on the x,y coordinates of the island centroids)

### in the mantel tests below, the way to read the plots is that boxes above the lines are positively correlated, boxes below the lines are negatively correlated
### filled boxes are significant; empty boxes are not (see also the output)
### the distances are in meters

north3plusJaccard = vegdist(threePlusNorth_hungerfordia, method = "jaccard"); 
north3plusIslandEuclid = dist(threePlusNorth_islandData$YM, threePlusNorth_islandData$XM, method = "euclidean") 
north3plusCorrelogJaccard = mantel.correlog(north3plusJaccard, north3plusIslandEuclid, r.type = "kendal") 
north3plusCorrelogJaccard
plot(north3plusCorrelogJaccard)
#Spatial autocorrelation of Hungerfordia assemblages in the
#Northern part of Chelbacheb
#distance in meters

south3plusJaccard = vegdist(threePlusSouth_hungerfordia, method = "jaccard"); 
south3plusIslandEuclid = dist(threePlusSouth_islandData$YM, threePlusSouth_islandData$XM, method = "euclidean") 
south3plusCorrelogJaccard = mantel.correlog(south3plusJaccard, south3plusIslandEuclid, r.type = "kendal") 
south3plusCorrelogJaccard
plot(south3plusCorrelogJaccard)
#Spatial autocorrelation of Hungerfordia assemblages in the
#Southern part of Chelbacheb
#distance in meters 


### another pattern to look for in beta diversity is nestedness
### let's see if the northern and southern portions of the limestone islands of Chelbacheb have different patterns of nestedness

northernHungerNestPlot = nestedtemp(threePlusNorth_hungerfordia); 
plot(northernHungerNestPlot, kind = "incidence", col = c("black", "lightgreen"))
plot(northernHungerNestPlot, col=rev(terrain.colors(100)))

southernHungerNestPlot = nestedtemp(threePlusSouth_hungerfordia)
plot(southernHungerNestPlot, kind = "incidence", col = c("black", "lightgreen"))
plot(southernHungerNestPlot, col=rev(terrain.colors(100)))

### in library "betapart" we can ask whether one part of the archipelago is significantly more nested within a hypothesis -testing framework

betaHungerSouth = beta.sample(south_hungerfordia, index.family="sor", sites=14, samples=1000)
betaHungerNorth = beta.sample(north_hungerfordia, index.family="sor", sites=14, samples=1000)
plot(density(betaHungerNorth$sampled.values$beta.SIM), col="red", xlim=c(0,1))
lines(density(betaHungerSouth$sampled.values$beta.SIM), col="blue")
p.value.beta.SIM = length( which (betaHungerSouth$sampled.values$beta.SIM > betaHungerNorth$sampled.values$beta.SIM))/1000
p.value.beta.SIM

### but be careful, because it seems like this hypothesis test is set up in such a way that the number of sites you're subsampling is really important
### at least with my data... I think this stems from the fact that with fewer total sites, you can lose the signal of nestedness
### in the southern Chelbacheb because there are still some islands with single-island endemics and very few species
### so neither assemblage is perfectly nested, but with enough sites the signal of nestedness comes through in the southern lagoon
### there are 17 and 19 sites in the southern and northern subsets, respectively
### what happens when you repeat the steps above for "beta.sample" but ....
### change "sites = 14" to a lower number? (between, say, 8 and 12)


### now let's check out zeta diversity: 

### how do the plots below compare with the respective plots for spatial autocorrelation you did previously? 

southXY = south_islandData[, 1:2]
zetaDecay_SouthHunger = Zeta.ddecay(southXY, south_hungerfordia,  sam = 100, order = 3, normalize = "Jaccard", confint.level = 0.95,plot=FALSE)
dev.new()
Plot.zeta.ddecay(zetaDecay_SouthHunger)

northXY = north_islandData[, 1:2]
zetaDecay_NorthHunger = Zeta.ddecay(northXY, north_hungerfordia,  sam = 100, order = 3, normalize = "Jaccard", confint.level = 0.95, plot=FALSE)
dev.new()
Plot.zeta.ddecay(zetaDecay_NorthHunger)