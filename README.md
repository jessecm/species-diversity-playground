The general goal of the Species Diversity Playground is to give folks a chance to 
take different R tools that ecologists use to quantify species diversity patterns out 
for a spin. Especially folks in the fall 2019 EFB 496/796 course at SUNY-ESF 
(course website here: https://www.jessecm.org/efb496796/ )

The intro_diversityMetrics file introduces some basics of R, as well as some simple 
instances of the Simpson and Shannon diversity indices using "vegan", "lattice", and "iNEXT".

The  2019_ efb496796_ divPlayground files are all self-contained instances that take the user through the 
creation of a spatially explicit, fake community that can be visualized and then 
used as a jumping-off point to explore how different metrics of diversity deal
quantify the patterns. Most of the functions involved are from the R package "vegan," with
additional functions from "meteR," "betapart," and others. 

The betaDiversity _hungerfordia _realData.R file contains scripts for analyzing real 
beta (and zeta) diversity patterns data for the landsnail genus *Hungerfordia* in the Palau
archipelago. The user will have to read in the .txt data files in the 
"palauHungerfordiaDataFiles" directory. These data are from a series of taxonomic 
monographs by Yamazaki et al. 

The  midDomainEffect.R file explores a way of generating (as you might guess) a mid-domain effect, 
which is a somewhat plausible way that a latitudinal diversity gradient might arise (see e.g., 
Colwell & Lees 2000). 

Further reading: 

Baselga, A. & Orme, C.D.L. (2012) betapart: an R package for the study of beta diversity. 
    Methods in ecology and evolution / British Ecological Society, 3, 808–812.
    
Colwell, R.K. & Lees, D.C. (2000) The mid-domain effect: geometric constraints on the 
    geography of species richness. Trends in ecology & evolution, 15, 70–76.
    
Hsieh, T.C., Ma, K.H., & Chao, A. (2016) iNEXT: an R package for rarefaction and 
    extrapolation of species diversity (H ill numbers). Methods in ecology and evolution, 
    7, 1451–1456.

Latombe, G., McGeoch, M.A., Nipperess, D.A., & Hui, C. (2018) zetadiv: an R package 
    for computing compositional change across multiple sites, assemblages or cases. bioRxiv, 324897.
    
Oksanen, J., Blanchet, F.G., Friendly, M., Kindt, R., Legendre, P., McGlinn, D., 
    Minchin, P.R., O’Hara, R.B., Simpson, G.L., Solymos, P., Stevens, M.H.H., 
    Szoecs, E., & Wagner, H. (2016) Vegan: Community Ecology Package version 2.4. 
    Available: http://cran.r-project.org/, http://vegan.r-forge.r-project.org/ .
    
Rominger, A.J. & Merow, C. (2017) meteR: an r package for testing the maximum entropy theory of ecology. 
    Methods in ecology and evolution / British Ecological Society, 241–247.
    
Yamazaki, K., Yamazaki, M., & Ueshima, R. (2013) Systematic review of diplommatinid 
    land snails (Caenogastropoda, Diplommatinidae) endemic to the Palau Islands. 
    (1) Generic classification and revision of Hungerfordia species with highly 
    developed axial ribs. Zootaxa, 3743, 1–71.
    
Yamazaki, K., Yamazaki, M., & Ueshima, R. (2015a) Systematic review of diplommatinid 
    land snails (Caenogastropoda, Diplommatinidae) endemic to the Palau Islands. 
    (2) Taxonomic revision of Hungerfordia species with low axial ribs. Zootaxa, 3976, 1–89.
    
Yamazaki, M., Yamazaki, K., Rundell, R.J., & Ueshima, R. (2015b) Systematic review of 
    diplommatinid land snails (Caenogastropoda, Diplommatinidae) endemic to the Palau Islands. 
    (3) Description of eight new species and two new subspecies of Hungerfordia. Zootaxa, 4057, 511–538.